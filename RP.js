const fs = require('fs'); //for file system handling
const axios = require('axios'); //3rd party rest library
const { prompt, Input } = require('enquirer'); //for user input
const Store = require('data-store'); //for command history
const { exit } = require('process'); //to kill the script on command

//common variables
let rpConfig;
let currentWebhook;
let currentCharacter;
let currentName;
let currentAvatar;
let currentThread;

//rpConfig file handling
const loadRpConfig = function () 
{ 
    rpConfig = JSON.parse(fs.readFileSync('RP Config.json')); 
    console.log("Discord RP Helper.  " + (rpConfig.webhooks?.length || 0) + " Webhooks, " + (rpConfig.characters?.length || 0) + " Characters loaded.");
}
const saveRpConfig = function () { 
    fs.writeFileSync('RP Config.json', JSON.stringify(rpConfig)); 
    loadRpConfig(); //immediately reload the config we just saved to make sure data in memory stays in sync with data on disc
}

//selects a webhook to use.  If 'preselect' is provided, that is used as the user's selection.  Otherwise a prompt is given.
const webhookSelect = async function(preselect)
{
    //remove current selection
    currentWebhook = null; 
    currentThread = null;

    const validChoices = rpConfig.webhooks.map(webhook => webhook.name);

    //only prompt if a name wasn't already given
    let selection = null;

    if (validChoices.includes(preselect)) {
        selection = preselect;
    }
    else {
        let webhookPromptResponse = await prompt({
            type:"select",
            name:"selection",
            message:"Select a webhook",
            initial: preselect,
            choices: validChoices
        });

        selection = webhookPromptResponse.selection;
    }

    //find the selected webhook
    for (let i = 0; i < rpConfig.webhooks.length; i++)
        if (rpConfig.webhooks[i].name === selection)
            currentWebhook = rpConfig.webhooks[i];

    //hopefully unreachable input validation
    if (!currentWebhook)
        console.log("Invalid Webhook.");
}

//selects a character to use.  If 'preselect' is provided, that is used as the user's selection.  Otherwise a prompt is given.
const characterSelect = async function(preselect)
{
    //remove current selection
    currentCharacter = null; 
    currentAvatar = null;
    currentName = null;

    const validChoices = rpConfig.characters.map(char => char.name);

    //only prompt if a name wasn't already given
    let selection = null;

    if (validChoices.includes(preselect)) {
        selection = preselect;
    }
    else {
        let characterPromptResponse = await prompt({
            type:"autocomplete",
            name:"selection",
            message:"Select a character",
            choices: validChoices
        });

        selection = characterPromptResponse.selection;
    }

    //find the selected character
    for (let i = 0; i < rpConfig.characters.length; i++)
        if (rpConfig.characters[i].name === selection)
            currentCharacter = rpConfig.characters[i];

    //hopefully unreachable input validation
    if (!currentCharacter)
        console.log("Invalid Character.");

    //set the name/avatar based on selected character
    currentName = currentCharacter.name;
    currentAvatar = currentCharacter.url;
}

//selects a thread number.  If 'preselect' is provided, that is used as the user's selection.  Otherwise a prompt is given.  0 can be used to quit using threads
const threadSelect = async function(preselect)
{
    //only promt if a name wasn't already given
    let selection = null;

    if (preselect) {
        selection = preselect;
    }
    else {
        let threadPromptResponse = await prompt({
            type:"input",
            message: "Thread ID (With dev mode, right click thread -> copy ID)",
            name:"thread"
        });

        console.log(threadPromptResponse);

        selection = threadPromptResponse.thread;
        if (selection === 0)
            selection = null;
    }
    currentThread = selection;
}

//sends an RP chat message
const sendRP = async function(message)
{
    const url=currentWebhook.url + (currentThread? ("?thread_id=" + currentThread) : "");
    // console.log(url)
    return axios.post(url, {
        "username": currentName,
        "avatar_url": currentAvatar,
        "content": message
    }).then(function (res) {}) //do nothing on success
    .catch(function (err) { 
        //show errors
        if (err.response)
        {
            console.error(err.response?.status + ": " + err.response?.statusText);
            console.error(err.response?.data); 
        }
        else
        {
            console.error(err.message);
        }
    }); 
}

//edits an existing RP chat message
const editMessage = async function(messageId, newBody)
{
    const url=currentWebhook.url + "/messages/" + messageId;
    return axios.patch(url, {
        "content": newBody
    }).then(function (res) {}) //do nothing on success
    .catch(function (err) { 
        //show errors
        if (err.response)
        {
            console.error(err.response?.status + ": " + err.response?.statusText);
            console.error(err.response?.data); 
        }
        else
        {
            console.error(err.message);
        }
    }); 
}

//shows an interactive setup menu for adding/modifying/removing characters and webhooks
const setupMenu = async function() {
    while (true)
    {
        const setupMenuResponse = await prompt({
            type:"select",
            name:"selection",
            message:"Setup Menu",
            choices: ["Add Webhook","Rename Webhook","Remove Webhook","Add Character","Rename Character","Modify Character","Remove Character","Exit Setup"]
        });
        console.log(setupMenuResponse.selection);
        switch (setupMenuResponse.selection)
        {
            case "Add Webhook":
                currentCharacter = null;
                currentAvatar = null;
                currentName = "RP Helper Test Post"

                let done = false;
                while (!done)
                {
                    const formResponse = await prompt({
                        type:"form",
                        name:"webhook",
                        message:"URL can be found in the discord channel integration settings.  If you do not have access to that, contact an admin of your discord server",
                        choices: [
                            {name:"name",message:"Name"},
                            {name:"url",message:"URL"}
                        ]
                    });

                    if (rpConfig.webhooks.find(wh => wh.name === formResponse.webhook.name))
                    {
                        console.log("A webhook by that name already exists.");
                        break;
                    }

                    currentWebhook = formResponse.webhook;
                    await sendRP("Test Post Please Ignore");

                    const testPostResponse = await prompt({
                        type:"select",
                        message:"Did a test message appear in the corret channel?",
                        choices:["Yes","Retry","Cancel"],
                        name:"response"
                    });
                
                    if (testPostResponse.response === "Yes")
                    {
                        rpConfig.webhooks.push(currentWebhook);
                        saveRpConfig();
                        console.log("Webhook added.");
                    }

                    if (testPostResponse.response !== "Retry")
                        done = true;
                }

                continue;

            case "Rename Webhook": 
                await webhookSelect();

                const renameWebhookPrompt = await prompt({
                    type:"input",
                    message:"Choose a new name for " + currentWebhook.name,
                    name:"newName"
                });

                if (rpConfig.webhooks.find(wh => wh.name === renameWebhookPrompt.newName))
                {
                    console.log("A webhook by that name already exists.");
                    break;
                }

                const newWebhook = {
                    name: renameWebhookPrompt.newName,
                    url: currentWebhook.url
                }

                const indexOfCurrentWebhook = rpConfig.webhooks.indexOf(currentWebhook);
                rpConfig.webhooks.splice(indexOfCurrentWebhook,1,newWebhook);
                saveRpConfig();
                
                console.log("Webhook renamed.");

                currentWebhook = null;
                currentThread = null;
                continue;

            case "Remove Webhook": 
                await webhookSelect();

                const removeWebhookConfirmPrompt = await prompt({
                    type:"confirm",
                    name:"response",
                    message:"Are you sure you want to remove " + currentWebhook.name + "?"
                });

                if (removeWebhookConfirmPrompt.response)
                {
                    const indexToRemove = rpConfig.webhooks.indexOf(currentWebhook);
                    rpConfig.webhooks.splice(indexToRemove,1);
                    saveRpConfig();
                    console.log("Webhook removed");
                }

                currentWebhook = null;
                currentThread = null;
                continue;

            case "Add Character": 
                currentCharacter = null;
                currentAvatar = null;
                currentName = null;

                let addCharacterDone = false;
                while (!addCharacterDone)
                {
                    const formResponse = await prompt({
                        type:"form",
                        name:"character",
                        message:"URL can be found in the discord channel integration settings.  If you do not have access to that, contact an admin of your discord server",
                        choices: [
                            {name:"name",message:"Character Name"},
                            {name:"url",message:"Avatar URL"}
                        ]
                    });

                    if (rpConfig.characters.find(ch => ch.name === formResponse.character.name))
                    {
                        console.log("A character by that name already exists.");
                        break;
                    }

                    console.log("Select a webhook that will be used to test the new character");
                    await webhookSelect();

                    currentCharacter = formResponse.character;
                    currentAvatar = currentCharacter.url;
                    currentName = currentCharacter.name;
                    
                    await sendRP("Test Post Please Ignore");

                    const testPostResponse = await prompt({
                        type:"select",
                        message:"Did the test message appear with the correct name and avatar?",
                        choices:["Yes","Retry","Cancel"],
                        name:"response"
                    });
                
                    if (testPostResponse.response === "Yes")
                    {
                        rpConfig.characters.push(currentCharacter);
                        saveRpConfig();
                        console.log("Webhook added.");
                    }

                    if (testPostResponse.response !== "Retry")
                        addCharacterDone = true;
                }

                continue;

            case "Modify Character": 
                await characterSelect();
                const indexOfOldCharacter = rpConfig.characters.indexOf(currentCharacter);    

                let modifyCharacterDone = false;
                while (!modifyCharacterDone)
                {
                    const formResponse = await prompt({
                        type:"form",
                        name:"character",
                        message:"URL can be found in the discord channel integration settings.  If you do not have access to that, contact an admin of your discord server",
                        choices: [
                            {name:"name",message:"Character Name",initial:currentCharacter.name},
                            {name:"url",message:"Avatar URL",initial:currentCharacter.url}
                        ]
                    });

                    console.log("Select a webhook that will be used to test the modified character");
                    await webhookSelect();

                    currentCharacter = formResponse.character;
                    currentAvatar = currentCharacter.url;
                    currentName = currentCharacter.name;
                    
                    await sendRP("Test Post Please Ignore");

                    const testPostResponse = await prompt({
                        type:"select",
                        message:"Did the test message appear with the correct name and avatar?",
                        choices:["Yes","Retry","Cancel"],
                        name:"response"
                    });
                
                    if (testPostResponse.response === "Yes")
                    {
                        rpConfig.characters.splice(indexOfOldCharacter,1,currentCharacter);
                        saveRpConfig();
                        console.log("Webhook modified.");
                    }

                    if (testPostResponse.response !== "Retry")
                        modifyCharacterDone = true;
                }

                continue;

            case "Remove Character": 
                await characterSelect();

                const removeCharacterConfirmPrompt = await prompt({
                    type:"confirm",
                    name:"response",
                    message:"Are you sure you want to remove " + currentCharacter.name + "?"
                });

                if (removeCharacterConfirmPrompt.response)
                {
                    const indexToRemove = rpConfig.characters.indexOf(currentCharacter);
                    rpConfig.characters.splice(indexToRemove,1);
                    saveRpConfig();
                    currentCharacter = null;
                    console.log("Character removed");
                }

                currentCharacter = null;
                continue;

            case "Exit Setup":
                return;
        }
    }
}

//This is the main loop function that runs over and over until the script ends
const mainMenu = async function () {
    while (true) {
        let rpStatusText = 
            (currentWebhook?.name || '(no Webhook)') + //show name of the current webhook (or placeholder if there is none)
            (currentThread? "[" + currentThread + "]" : "") + 
            " | " + //visual separator
            (currentName || '(no name)') + //show name of the current character (or placeholder if there is none)
            (currentCharacter && (currentCharacter.name!==currentName) ? '*' : ''); //add a '*' if the name being used doesn't match the currently selected character

        process.title=rpStatusText; //update winddow title

        //prompt user
        let mainLoopInput = await prompt({
            type:"input",
            message: rpStatusText,
            name:"text",
            history: {
                store: new Store({ path: `${__dirname}/command history.json` }),
                autosave: true
            }
        });

        //input is null when user hits ctrl+C
        if (!mainLoopInput)
            exit();

        if (mainLoopInput.text.startsWith('/') || mainLoopInput.text.startsWith('\\')) { //treat lines starting with '/' or '\' as a command
            const command = mainLoopInput.text.substr(1).split(' '); //remove the leading command flag and then separate args into an array
            switch (command[0]) {
                //reload config
                case "reload":
                case "r":
                    currentWebhook = null;
                    currentThread = null;
                    currentAvatar = null;
                    currentName = null;
                    currentCharacter = null;
                    loadRpConfig();
                    continue;
                
                //pick a webhook
                case "w":
                case "wh":
                case "webhook":
                    await webhookSelect(command[1]);
                    continue;

                //pick a character
                case "c":
                case "ch":
                case "char":
                case "character":
                    await characterSelect(command[1]);
                    continue;

                case "t":
                case "th":
                case "thread":
                    await threadSelect(command[1]);
                    continue;

                //pick a name
                case "n":
                case "name":
                    currentName = null; //remove current selection

                    //only prompt if a name wasn't already given
                    if (command[1])
                    {
                        currentName = command[1];
                    }
                    else
                    {
                        let namePrompt = await prompt({
                            type:"input",
                            message:"Name?",
                            name:"selection"
                        });
                        currentName = namePrompt.selection;
                    }

                    continue;

                //edit an existing message
                case "e":
                case "edit":
                    const msgid = command[1];
                    const newBody = command.slice(2).join(' ');
                    await editMessage(msgid, newBody.replace(/\\n/g,"\n")); //regular expression is to turn \n into actual line breaks for discord
                    continue;

                //to end the script
                case "x":
                case "q":
                case "exit":
                case "quit":
                    exit(0);

                //shows the readme
                case "?":
                case "help":
                    console.log(fs.readFileSync("readme.txt").toString());
                    continue;

                //shows the setup menu for interactively adding/removing characters/webhooks
                case "setup":
                    await setupMenu();
                    continue;

                //everything else
                default:
                    console.log("unknown command");
                    console.debug(command);
            }
        }
        else {
            //interpret this as chat.  First, some validations:
            if (!mainLoopInput.text) continue; //skip with no message if zero input

            //enforce we have at least a webhook and a name
            if (!currentWebhook) {
                console.log("Need to choose a webhook first (/webhook, /w)");
                continue;
            }
            if (!currentName) {
                console.log("Set a name (/name, /n) or a character (/character, /char, /c)");
                continue;
            }

            //now we can send an actual request.
            await sendRP(mainLoopInput.text.replace(/\\n/g,"\n")); //regular expression is to turn \n into actual line breaks for discord
        }
    }
}

//EXECUTION STARTS HERE

//initial setup
loadRpConfig();
if (rpConfig.defaultWebhook)
    for (let i = 0; i < rpConfig.webhooks.length; i++)
        if (rpConfig.webhooks[i].name === rpConfig.defaultWebhook)
            currentWebhook = rpConfig.webhooks[i]

//show the main menu
mainMenu();