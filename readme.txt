Discord RP Helper is a simple node script designed to help with posting as various alternative characters via discord webhooks.

Before first use, copy or rename "RP Config example.json" to "RP Config.json", then follow the example format to list out the names and URLs for every webhook and character you want to use.  If you prefer, the script itself also has an interactive setup menu you can use instead of modifying the JSON directly.  Simply type '/setup' to get started.  You will also need to install Node, which you can find at nodejs.org.  Once node is installed, simply go to the folder you saved this script in a command prompt and run "node RP.js".

Webhooks can be created from the integration settings on the discord channel.  If you do not have access to this, ask an admin on your discord server to provide you the webhook URL(s) for each channel you want to RP in.

Anything you enter starting with a '/' or '\' will be treated as a command (see below).  Anything else you enter will be sent directly into discord, using the specified webhook.  The post name avatar will be taken from the image URL in the config file. 

/reload, /r
    Reloads the webhook/character data from the config file

/edit, /e
    Modify an existing message.  First argument is the message ID and the rest is treated as the new message body.

/w, /wh, /webhook
    selects the webhook to use, through an interactive menu.  As a shortcut you can also specify your choice in the command (i.e, "/w myWebhook")

/t, /th, /thread
    selects the thread to post to.  In order to use threads, first make sure the webhook is set to the channel which "owns" the thread you want to post in.  Then right click on the thread in your sidebar and choose "Copy ID".  The number Discord places on your clipboard is what you give to this command in order to post in the thread.  If the "Copy ID" option is missing, turn on developer mode in discord (User Settings -> Advanced -> Developer mode)

/c, /ch, /char, /character
    selects the character to use, through an interactive menu.  As a shortcut you can also specify your choice in the command (i.e, "/c Billy")

/n, /name
    overrides the character name without changing the avatar.  Useful if multiple characters chat with the same avatar or if you want to temporarily alter the name for some other reason.  When this is on, a * will appear next to the character name inside the script.  To turn it off, simply select a character again.

/x, /q, /exit, /quit
    exits the script.  

/setup
    opens an interactive setup menu to add/modify/remove webhooks and characters

/?, /help
    Shows this readme.

When not in a submenu, you can access a command history by holding ALT and pressing the up/down arrow keys on your keyboard.